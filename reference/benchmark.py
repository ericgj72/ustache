
import platform
import textwrap
import timeit

import chevron

import ustache


jit = platform.python_implementation() in (
    'PyPy',
    'Pyston',
    )
template = textwrap.dedent('''
    variable: {{value}}
    escaped: {{literal}}
    unescaped: {{{literal}}} and {{&literal}}
    block: {{#nested}}{{value}} and {{#lambda}}{{value}}{{/lambda}}{{/nested}}
    partial on values: {{#values}}{{>partial}}{{/values}}
    missing partial: {{>missing}}
    {{=(( ))=}}tag switch: {{Hello ((world))}}((={{ }}=))
    --- {{!
        we do things faster
        }}
    values: {{#values}}{{.}}{{/values}}{{^values}}falsy{{/values}}
    values.0: {{#values.0}}{{.}}{{/values.0}}{{^values.0}}falsy{{/values.0}}
    empty: {{#empty}}{{.}}{{/empty}}{{^empty}}falsy{{/empty}}
    empty.0: {{#empty.0}}{{.}}{{/empty.0}}{{^empty.0}}falsy{{/empty.0}}
    {{.}}
    {{#arrays}}[{{#.}}{{.}}{{/.}}] is {{^.}}empty{{/.}}{{#0}}non empty{{/0}}
    {{/arrays}}
    ''').strip()
values = {
    'world': 'World!',
    'value': 'value',
    'nested': {
        'value': 'nested value',
        },
    'values': [1, 2, 3],
    'arrays': [[1, 2, 3], []],
    'literal': '<!-- literal -->',
    'empty': [],
    'lambda': lambda template, render: render(
        'lambda:{!r}'.format(
            template.decode()
            if isinstance(template, bytes) else
            template
            )),
    }
partials = {
    'partial': 'partial:{{.}} ',
    }
resolver = lambda template: partials.get(
    template
    if isinstance(template, str) else
    template.decode()
    )

template_bytes = template.encode()
number = 1000
tests = {
    'ustache, str': lambda: ustache.render(
        template,
        values,
        resolver=resolver,
        ),
    'ustache, str, no cache': lambda: ustache.render(
        template,
        values,
        cache={},
        resolver=resolver,
        ),
    'ustache, stream, str': lambda: ''.join(ustache.stream(
        template,
        values,
        resolver=resolver,
        )),
    'ustache, stream, str, no cache': lambda: ''.join(ustache.stream(
        template,
        values,
        cache={},
        resolver=resolver,
        )),
    'ustache, bytes': lambda: ustache.render(
        template_bytes,
        values,
        resolver=resolver,
        ),
    'ustache, bytes, no cache': lambda: ustache.render(
        template_bytes,
        values,
        cache={},
        resolver=resolver,
        ),
    'ustache, stream, bytes': lambda: b''.join(ustache.stream(
        template_bytes,
        values,
        resolver=resolver,
        )),
    'ustache, stream, bytes, no cache': lambda: b''.join(ustache.stream(
        template_bytes,
        values,
        cache={},
        resolver=resolver,
        )),
    'chevron': lambda: chevron.render(
        template,
        values,
        partials_dict=partials,
        ),
    }


def main():
    results = {}
    justify = max(map(len, tests))
    header = '{} | time (x{})'.format('test'.ljust(justify), number)
    right = len(header) - justify - 3
    print(header)
    print('{}-|-{}'.format('-' * justify, '-' * right))
    for name, test in tests.items():
        for _ in range(number if jit else 1):
            res = test()
        results[name] = res.decode() if isinstance(res, bytes) else res
        total = format(timeit.timeit(test, number=number), f'0.{right - 2}f')
        print(f'{name.ljust(justify)} | {total}')



if __name__ == '__main__':
    main()
