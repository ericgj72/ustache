import json
import sys

import ustache

def fnc(t, render):
    return render(t)

with open('test_data.json') as f:
    print(json.dumps([
        (t, v, ''.join(ustache.render(t, {**v, 'lambda': fnc})))
        for t, v in json.load(f)
        ]))
