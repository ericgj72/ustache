import json
import chevron

def fnc(t, render):
    return render(t)

with open('test_data.json') as f:
    print(json.dumps([
        (t, v, chevron.render(t, {**v, 'lambda': fnc}))
        for t, v in json.load(f)
        ]))
