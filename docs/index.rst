.. title:: Index

Documentation
=============

Welcome to ustache documentation.

ustache, Mustache for Python
----------------------------

ustache is a `Mustache template language`_ implementation for Python,
focused on high compatibility with JavaScript implementations,
with no dependencies other than the `Python Standard Library`_.

.. _mustache: https://mustache.github.io/
.. _stdlib: https://docs.python.org/3/library/index.html
.. _Mustache template language: mustache_
.. _Python Standard Library: stdlib_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Readme <README>
   Changelog <CHANGELOG>
   Tips and tricks <tips>
   API reference <ustache>
   License <LICENSE>


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
