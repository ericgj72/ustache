"""
Configuration file for the Sphinx documentation builder.

Reference: https://www.sphinx-doc.org/en/master/usage/configuration.html

"""

import ustache

project = ustache.__name__
copyright = f'2021, {ustache.__author__}'  # noqa
author = ustache.__author__
release = ustache.__version__
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinx.ext.coverage',
    'sphinx.ext.intersphinx',
    'sphinx.ext.autosectionlabel',
    'sphinx_autodoc_typehints',
    'recommonmark',
    ]
templates_path = ['_templates']
exclude_patterns = []
html_theme = 'alabaster'
html_static_path = ['_static']
html_sidebars = {
    '**': [
        'sidebar.html',
        'globaltoc.html',
        'relations.html',
        'sourcelink.html',
        'searchbox.html',
        ],
    }
source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
    '.txt': 'markdown',
    }
autodoc_default_options = {
    'members': True,
    'undoc-members': False,
    'no-undoc-members': True,
    # 'private-members': ','.join(()),
    # 'special-members': ','.join(()),
    'inherited-members': False,
    'show-inheritance': True,
    'member-order': 'bysource',
    # 'exclude-members': ','.join(()),
    }
intersphinx_mapping = {
    'python': ('https://docs.python.org/', None),
    }
autosectionlabel_prefix_document = True
